This project allows you to generate LaTeX solution files from a given input PDF exercise sheet.

![transform](https://git.rwth-aachen.de/tobias.schleifstein/LaTeXUtil/uploads/be61b1fca036f910b65bc40a812d8271/transform.png)

# Setup

```bash
# first add this project to your git repository
git submodule add https://git.rwth-aachen.de/tobias.schleifstein/LaTeXUtil.git
cp LaTeXUtil/module.tex.example module.tex

# run the script
LaTeXUtil/frame.py exercisesheet0.pdf
```

# Dependencies

The `frame.py` script only requires a minimal Python 3 installation and `poppler` for the `pdftotext` script.

## Linux

Install `python` and `poppler` from your favorite package manager.

## Windows

Go to https://www.archlinux.org/download/, then proceed like above.

## MacOS

```bash
brew install python3 poppler
```

## Docker

If you prefer to use containers, a Dockerfile is provided.

First build the container using `podman` or `docker`, then run it mounting your current directory:

```bash
podman build -t latexutil .
podman run -v "$PWD:/tmp/mnt" latexutil frame.py exercisesheet0.pdf
```

# FAQ

## Can this handle floating point numbers in exercise sheet points?

Yes.

## LaTeXUtil/frame.py: No such file or directory

You have cloned the repo without its submodules.
Run `git submodule update --init` to fix it.

## Can this project solve the exercises for me as well?

Unfortunately as of now, you have to come up with the solutions on your own.

# Acknowledgement

The documentclass is based on http://upload.watercrystal.net/latex/WCarticle/, though modified to support floating point numbers.
