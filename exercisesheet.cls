\NeedsTeXFormat{LaTeX2e}

\LoadClass{article}
\ProvidesClass{exercisesheet}

\setlength{\parindent}{0pt}

% Packages
\RequirePackage{geometry}
\geometry{
	a4paper,
	total = {210mm, 297mm},
	left = 22mm,
	right = 22mm,
	top = 20mm,
	bottom = 20mm,
}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{lmodern}
\RequirePackage{amsmath, amsfonts, amssymb, amsthm}
\RequirePackage{multicol}
\RequirePackage{listings}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\usetikzlibrary{shapes, arrows, positioning, patterns, decorations.pathreplacing}

\RequirePackage{changepage}
\RequirePackage{etoolbox}
\RequirePackage{environ}
\RequirePackage{fp}

% Counters
\newcounter{exercisenumber}
\newcounter{subexercisenumber}
\newcounter{subsubexercisenumber}
\FPset\totalpoints{0}

% Commands
\newcommand{\Exercise}{Exercise}
\newcommand{\ExerciseNumber}{\arabic{exercisenumber}}

\newcommand{\SubexerciseLeft}{(}
\newcommand{\SubexerciseNumber}{\alph{subexercisenumber}}
\newcommand{\SubexerciseRight}{)}

\newcommand{\SubsubexerciseLeft}{(}
\newcommand{\SubsubexerciseNumber}{\roman{subsubexercisenumber}}
\newcommand{\SubsubexerciseRight}{)}

% Module specific prefrences
\providecommand{\modulelocation}{module}
\input{\modulelocation}

% Environments
\newenvironment{exercise}[1]
{
	\FPset\points{#1}
	\stepcounter{exercisenumber}
	\setcounter{subexercisenumber}{0}
	\section*{\Exercise~\ExerciseNumber}
}
{

	\phantom{} \hfill A\arabic{exercisenumber}: \framebox[1.75cm]{\hspace{1cm}/\hfill\points} \\
	\FPadd\totalpoints\totalpoints\points
	\addPT{\arabic{exercisenumber}}{\points}
	\xdef\totalpoints{\totalpoints}
}

\newenvironment{subexercise}
{
	\stepcounter{subexercisenumber}
	\setcounter{subsubexercisenumber}{0}

	\ifnum\value{subexercisenumber}=1
	\else
		\vspace{1em}
	\fi

	\textbf{\SubexerciseLeft\SubexerciseNumber\SubexerciseRight}
	\begin{adjustwidth}{2em}{0em}
	\vspace{-\baselineskip}
}
{
	\end{adjustwidth}
}

\newenvironment{subsubexercise}
{
	\stepcounter{subsubexercisenumber}

	\ifnum\value{subsubexercisenumber}=1
	\else
		\vspace{1em}
	\fi

	\textbf{\SubsubexerciseLeft\SubsubexerciseNumber\SubsubexerciseRight}
	\begin{adjustwidth}{2em}{0em}
	\vspace{-\baselineskip}
}
{
	\end{adjustwidth}
}

\newcommand{\addPT}[2]
{
	    \edef\tempA{A#1 &}
	    \edef\tempB{\qquad/#2 &}

	\expandafter\gappto\expandafter\PTableA\expandafter{\tempA}
	\expandafter\gappto\expandafter\PTableB\expandafter{\tempB}
	\expandafter\gappto\expandafter\PTHead\expandafter{c | }
}

\newcommand{\pointtable}{
    \begin{table}[h!]
        \centering
        \ifcsname PTableASaved\endcsname
        \begin{tabular}{\PTHeadSaved c}
            \PTableASaved $\Sigma$ \\ \hline
        	\PTableBSaved \qquad / \totalpointsSaved
    	\end{tabular}
        \else
        Compile again\ClassWarning{Compile again}
        \fi
    \end{table}
}

\AtEndDocument{
    \makeatletter
    \immediate\write\@mainaux{\string\gdef\string\PTableASaved{\PTableA}}%
    \immediate\write\@mainaux{\string\gdef\string\PTableBSaved{\PTableB}}%
    \immediate\write\@mainaux{\string\gdef\string\PTHeadSaved{\PTHead}}%
    \FPeval\totalpoints{clip(totalpoints)}%
    \immediate\write\@mainaux{\string\gdef\string\totalpointsSaved{\totalpoints}}%
    \makeatother
}
