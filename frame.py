#!/usr/bin/python3

import sys
import re
import os
import subprocess
import argparse


# States that an exercise has inherently no points
NO_POINTS = -1
# States that an exercise should have points, but there was an error
INVALID_POINTS = -2

EXERCISE_REGEX = "^\s*(Aufgabe|Exercise|Programming Exercise)\s*[1-9]+\.?[1-9]*"
SUBEXERCISE_REGEX = "\s*\(?\s*[a-h]\s*\)"
SUBSUBEXERCISE_REGEX = "\s*\(?\s*[ivx]+\s*\)"


class Exercise:
    def __init__(self, name, points=NO_POINTS, formulaString=""):
        self.formulaString = formulaString
        self.points = points
        self.subexercises = []
        self.nestingLevel = 0
        self.name = name
        self.isReal = True

    def addSubexercise(self, subexercise, isReal=True, nesting=1):
        self.subexercises.append(subexercise)
        subexercise.nestingLevel = self.nestingLevel + nesting
        subexercise.isReal = isReal

    def hasRealSubexercise(self):
        return (len(self.subexercises) != 0 and self.subexercises[-1].isReal)

    def addSubsubexercise(self, subsubexercise):
        # Checks if the last subexercise is a real subexercise (i.e. basically an exercise with nesting level of exactly one more)
        if self.hasRealSubexercise():
            # Adds subsubexercise to real subexercise
            self.subexercises[-1].addSubexercise(subsubexercise)
        else:
            # Adds subsubexercise as unreal subexercise with higher nesting level
            self.addSubexercise(subsubexercise, False, 2)

    def toLaTeX(self):
        # Defines name of exercise based on nesting level
        if (self.nestingLevel == 0):
            exerciseName = "exercise"
        elif (self.nestingLevel == 1):
            exerciseName = "subexercise"
        else:
            exerciseName = "subsubexercise"

        # Defines indent level of exercise based on nesting level
        indentLevel = self.nestingLevel

        # Creates tabs string
        tabs = "\t" * indentLevel

        # Creates start of exercise definition
        pointsFormulaText = ""
        if self.points >= 0:
            # append the points, leaving out the unnecessary decimal point if it is an integer
            pointsText = "{:g}".format(self.points)
            formulaText = self.formulaString
            pointsFormulaText += " (" + formulaText
            if formulaText != pointsText:
                pointsFormulaText += "=" + pointsText
            pointsFormulaText += " points)"
        text = tabs + "% " + self.name + pointsFormulaText + "\n"
        text += tabs + "\\begin{" + exerciseName + "}"
        if (self.points == INVALID_POINTS):
            text += "{0}"
        elif (self.points != NO_POINTS):
            text += "{" + pointsText + "}"
        text += "\n"

        # Recursively creates string for subexercises
        for subexercise in self.subexercises:
            text += subexercise.toLaTeX() + "\n"

        # Creates end of exercise definition
        text += tabs + "\\end{" + exerciseName + "}"

        return text


def parseText(inputFile):
    # Creates dummy exercise for possible subexercises that get added before a real exercise exists
    dummyExercise = Exercise("Dummy Exercise", 0)
    exercises = [dummyExercise]

    # Iterates over the lines of the input file
    for line in inputFile:
        # Splits up line into a list of words
        words = line.split()

        if len(words) <= 0:
            continue

        # Matches exercise regex with line
        exerciseMatch = re.match(EXERCISE_REGEX, line)
        if exerciseMatch:
            # Gets exercise number
            exerciseGroup = exerciseMatch.group()
            numberMatch = re.match("[^\d]*(\d*\.?[1-9]*)", exerciseGroup)
            exerciseNumber = numberMatch.group(1)
            exerciseString = exerciseGroup.replace(exerciseNumber, '').strip()
            # Removes 'Aufgabe i' from words list
            words = line.replace(exerciseGroup, '').split()

            # Parses formula
            rawFormulaString = ""
            for word in words:
                rawFormulaString += word

            formulaString = re.sub("[^0-9\.\,\+=·]", "", rawFormulaString)
            if "=" in formulaString:
                formulaString = re.sub(r"=.*", "", formulaString)

            # force '.' as comma (as opposed to ',' because eval does not like it)
            formulaString = formulaString.replace(',', '.')

            # replace mid dot with asterisk to let eval correctly compute multiplications
            formulaString = formulaString.replace('·', '*')

            # Adds up all points
            try:
                formulaValue = eval(formulaString)
            except SyntaxError:
                print("Cannot evaluate points formula '" + formulaString + "' for exercise " + exerciseNumber)
                formulaValue = INVALID_POINTS

            exerciseNumberName = exerciseString + " " + exerciseNumber

            exercise = Exercise(exerciseNumberName, formulaValue, formulaString)
            exercises.append(exercise)
        # Matches subexercise regex with first word
        elif re.match(SUBEXERCISE_REGEX, words[0]):
            # Extracts number of subexercise (e.g. 'a')
            subexerciseNumber = re.sub("[()]", "", words[0])
            subexerciseNumberName = "(" + subexerciseNumber + ")"

            # Creates subexercise object and adds it to the data structure
            subexercise = Exercise(subexerciseNumberName)
            exercises[-1].addSubexercise(subexercise)
        # Matches subsubexercise regex with first word
        elif re.match(SUBSUBEXERCISE_REGEX, words[0]):
            # Extracts number of subsubexercise (e.g. 'i')
            subsubexerciseNumber = re.sub("[()]", "", words[0])
            subsubexerciseNumberName = "(" + subsubexerciseNumber + ")"

            # Creates subsubexercise object and adds it to the data structure
            subsubexercise = Exercise(subsubexerciseNumberName)
            if not exercises[-1].hasRealSubexercise():
                answer = input("Detected the subsubexercise " + subsubexerciseNumber + " before any subexercise in exercise " + exercises[-1].name + "."
                        + " What do you want to do?\n"
                        + "[1] Add as subexercise\n"
                        + "[2] Add as subsubexercise\n"
                        + "[3] Skip\n")
                if answer == "1":
                    print("1")
                    exercises[-1].addSubexercise(subsubexercise, False)
                elif answer == "2":
                    print("2")
                    exercises[-1].addSubsubexercise(subsubexercise)
            else:
                exercises[-1].addSubsubexercise(subsubexercise)

    # Removes dummy exercise if necessary
    if len(dummyExercise.subexercises) != 0:
        answer = input("There are subexercises before the existence of an exercise."
                + " Do you want to add a dummy exercise for them? (Otherwise they will be skipped) [y/N] ")
        if not(answer == "y" or answer == "Y" or answer == "yes" or answer == "Yes"):
            del exercises[0]
    else:
        del exercises[0]

    return exercises


def generateLaTeX(exercises, inputNumber, path, module_location):
    text = ""

    # Starts output text with LaTeX boilerplate
    # Only add the module path if it differs from the default
    if module_location != 'module':
        text += "\\newcommand{\\modulelocation}{" + str(module_location) + "}\n"

    text += "\\documentclass{" + path + "/exercisesheet}\n"
    text += "\n"
    text += "\\setcounter{section}{" + str(inputNumber) +"}\n"
    text += "\n"
    text += "\\begin{document}\n"
    text += "\\maketitle\n"
    text += "\\pointtable\n"
    text += "\n"

    # Converts exercises to LaTeX
    for exercise in exercises:
        text += exercise.toLaTeX() + "\n\n"

    # Closes LaTeX document
    text += "\\end{document}\n"

    return text


def main(argv):
    global environmentOpen, text

    # first parse CLI options
    parser = argparse.ArgumentParser(description='This script converts an input exercise PDF into a template latex solution file.')
    parser.add_argument('pdf_file', type=argparse.FileType('r'), help='Path to the input PDF file')
    parser.add_argument('-m', '--module', type=argparse.FileType('r'), help='Path to the module file', default='module.tex', metavar='FILE')
    parser.add_argument('-y', '--noconfirm', action='store_true', help='Do not ask for confirmation')
    args = parser.parse_args()

    # Saves path to pdf file
    PDFPath = args.pdf_file.name
    module_location = str(args.module.name)
    if module_location.endswith('.tex'):
        # latex requires the extension to be absent
        module_location = module_location[:-4]

    # Gets name and number of input file (e.g. the file "home01.pdf" has name "home01" and number "1")
    inputName = re.split("\.", os.path.split(PDFPath)[1])[0]
    try:
        inputNumber = int(re.sub("\D", "", inputName))
    except ValueError:
        print("Cannot determine number of exercise sheet!")
        inputNumber = 0

    # Creates name of output
    outputName = "u" + str(inputNumber).zfill(2)
    outputEnding = ".tex"

    # Checks if output file already exists
    if os.path.exists("./" + outputName + outputEnding):
        print("Warning: File \'" + outputName + outputEnding + "\' already exists!")
        if not args.noconfirm:
            answer = input("Do you want to override it? [y/N] ")
            if not(answer == "y" or answer == "Y" or answer == "yes" or answer == "Yes"):
                exit()

    # Converts pdf to txt with pdftotext
    pdftotext = subprocess.run(['pdftotext', '-layout', '-eol', 'unix', '-nopgbrk', PDFPath, '-'], text=True, capture_output=True)
    if pdftotext.returncode != 0:
        print("Cannot convert pdf to text!")
        exit()

    inputFile = pdftotext.stdout.splitlines()

    # Gets execution path
    path = os.path.dirname(argv[0])

    # Generates
    exercises = parseText(inputFile)
    text = generateLaTeX(exercises, inputNumber, path, module_location)

    # Opens output file
    outputFile = open(outputName + outputEnding, "w")

    # Writes file
    outputFile.write(text)

    # Closes output file
    outputFile.close()


if __name__ == "__main__":
    main(sys.argv)
