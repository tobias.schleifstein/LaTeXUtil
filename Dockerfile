FROM archlinux/base

RUN pacman -Syu --noconfirm poppler python git
RUN git clone https://git.rwth-aachen.de/tobias.schleifstein/LaTeXUtil.git /tmp/LaTeXUtil
ENV PATH="/tmp/LaTeXUtil:${PATH}"
WORKDIR /tmp/mnt
